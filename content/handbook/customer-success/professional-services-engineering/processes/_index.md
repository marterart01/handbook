---
title: "Processes and Methodology"
description: "Learn the processes and methodology that GitLab Professional Services uses to help ensure Customer Success."
---

## PS Process & Methodology Mapped to the Customer Journey

The Professional Services process and methodology fits within the Customer journey that is supported by Customer Success.Professional Services contributes to the customer journey from the point of **SOW Close** through the **Project Closee** phase.

![<!--!\[''\](/handbook/customer-success/professional-services-engineering/processes/customer-journey-mapped-ps-process.png)-->](<PS Delivery Customer Journey Flow - Page 1 (11).png>)

[Source, GitLab Team Members Only](https://docs.google.com/presentation/d/1eC_ocJkzNkH4Vw3v4Vkd3S58a0NALYxXtnb6BZ7pJdc/edit?usp=sharing)

## PS Process Methodology Stages

The above diagram (slide 4) is meant to describe the Directly Responsible Individuals (DRIs), Activities, Outcomes, and Tools/Collateral for each stage of the methodology. We can also see clear categorization of stages in pre-sales and post-sales phases.

In the linked pages below, you can see a detailed drill down into the steps within each stage that individuals use to perform activities to deliver desired outcomes per each stage. These pages are split by the Phase of the selling process (Pre-sales vs Post-sales).

### Scoping (Pre-Sales)

For a more detailed explanation of the steps that comprise each stage of the scoping phase, check out the [Scoping (Pre-Sales)](pre-sales-methodology) page. In this page, we drill down into- and describe- specific steps in each phase of the pre-sales scoping process.

![Pre-Sales Stages & Steps](pre-sales-methodology/scoping-workflow.png)

### Delivery (Post-Sales)

For a more detailed explanation of the steps that comprise each stage of the delivery phase, check out the [Delivery (Post-Sales)](post-sales-methodology) page. In this page, we drill down into- and describe- specific steps in each phase of the post-sales delivery process.

![Post-Sales Stages & Steps](post-sales-methodology/PS-delivery-workflow.png)
