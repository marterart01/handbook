---

title: "CSM Onboarding"
---







View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM-related handbook pages.

---

Onboarding for Customer Success Managers is a guided, methodical process to ensure new team members have the knowledge they need to be effective.

There is a lot to learn to make you a great GitLab Customer Success Manager. It is important for new team members to gain competency on how our platform solutions provide customer value, and how we partner with customers to build a productive relationship.

## Support and our Single Source of Truth

Since GitLab is a [handbook first](/handbook/company/culture/all-remote/handbook-first-documentation/) organization, the answer to any question you may have particularly throughout your first thirty days should be documented and readily available as this is our [Single Source of Truth (SSoT)](/handbook/values/#single-source-of-truth).

If you are unable to find the information you are looking for, please be sure to reach out to your Manager; Onboarding Buddy; People Connect Team member or Enablement Program Manager for support by tagging them in the comments section of this issue. If you need to troubleshoot specific issues with tools, you can add a screenshot of the issue directly in the comment to help them diagnose the problem.

Alternatively once you are active on [Slack](/handbook/communication/chat), which is used solely for informal communication, you will notice that you have been automatically added to a handful of useful support channels such #questions, #cs-questions, #solutions-architects, #it-help, etc.

## 30-60-90 Day Program

As a task in your onboarding issue, you will directed to our learning platform, Level up, to enroll in a CSM onboarding pathway based on your respective CSM Segment. Learning modules in this course provide guidance as to where you can find the processes & tools needed during your first months at GitLab.

## Meeting shadowing

To learn how we conduct both customer meetings and internal planning meetings, a new team member will shadow these meetings with one or more established members of the team.

[Meeting Shadowing](/handbook/customer-success/csm/csm-onboarding/shadowing/)

## CSM Office Hours

We host weekly CSM office hours meetings to help you answer questions in a judgment-free space. You can add your questions to the agenda document linked in the Google Calendar invite and hop on the call to work through questions or learn how to find helpful resources.

## Helpful resources for faster onboarding

By talking to new hires and recent new CSM team members, it was identified that the onboarding buddy relationship plays a critical role for success of a new hire in the CSM team. Existing CSMs explained that having a very close / short loop to their onboarding saw a close relationship as a tremendous speed up of their onboarding.

Buddies should always be CSMs (same department), to match the role as close as possible, as it will help the new hire to resolve questions around their responsibilities and tasks quicker and more efficient, compared to going through multiple chats or calls. To pick an onboarding buddy, CSM managers [will use the guidance section for buddies](/handbook/people-group/general-onboarding/onboarding-buddies/#managers-who-should-i-pick-as-an-onboarding-buddy) from our general handbook section.

### General

Following responsibilities should be seen as a priority for the onboarding buddy:

- The buddy should join the first 5-10 customer meetings led by the new joiner.
  - It is not only for technical guidance but much more for general **feedback and building confidence in the CSM role**.
  - The close feedback loop will enable new CSMs to learn faster by helping them understand if their approach was the best one, where they can improve, and what went well.
- Generally, increased interaction between the onboarding buddy and new CSM will reduce the onboarding time.
  - Asking questions and feeling comfortable doing so, improved new hire satisfaction to start working with clients

### SLACK Channels

New team members often struggle to find the right set of slack channels to participate in, and to filter out noise that is not relevant to them, especially in the first weeks of their start in the CSM team. The following list should provide an initial set of channels that will be helpful to new hires.

- **CSM - General**
  - #cs-questions
  - #customer-success
  - #sales-support
  - #field-fyi
  - #csm-updates
  - #tim-tams
- **GitLab - General**
  - #questions
  - #whats-happening-at-gitlab
  - #company-fyi
  - #it-help
  - #ai_field_strategy
  - #people-connect
  - #escalated_customers
  - #thanks
  - #whats-happening-at-gitlab
- **CSM - Regional - EMEA / DACH Team**
  - #team-csm-dach
  - #emea-customer-success
  - #team-csm-emea
  - #social-csm-emea

For all product management related questions, a CSM should review our handbook page at [following location](/handbook/product/categories/features/) to identify the right channel for the topic they are looking to learn more. Those team channels should be asked if there is no documentation available for the problem/question and also if the CSM did not find an issue that already discusses this question.

A great resource to learn how to engage with GitLab product is [available in the product handbook here](/handbook/product/how-to-engage/)

### Handbook

Collection of key handbook pages for CSMs to read and focus on when onboarding in their first 3 months

- [CSM Onboarding at GitLab](https://handbook.gitlab.com/handbook/customer-success/csm/csm-onboarding/)
- [Responsibilities and Services of a CSM at GitLab](https://handbook.gitlab.com/handbook/customer-success/csm/services/#responsibilities-and-services)
- [Using Gainsight](https://handbook.gitlab.com/handbook/customer-success/csm/gainsight/)
- [Searching GitLab like a pro](https://handbook.gitlab.com/handbook/tools-and-tips/searching/)
- [Answer client questions](https://handbook.gitlab.com/handbook/customer-success/csm/researching-customer-questions/)
- [Process for CSM lead account escalations](https://handbook.gitlab.com/handbook/customer-success/csm/escalations/)
- [STAR - Process for managing support tickets that, due to the situation that is critical for the customer’s business, require our attention](https://handbook.gitlab.com/handbook/support/internal-support/support-ticket-attention-requests/)
- [Process for CSM to CSM Account Handoff](https://handbook.gitlab.com/handbook/customer-success/csm/account-handoff/)
- [Available development paths and resources for Customer Success Managers at GitLab](https://handbook.gitlab.com/handbook/customer-success/csm/csm-development/)
- [DevOps - Foundations Learning](https://handbook.gitlab.com/handbook/customer-success/csm/csm-development/#devops)

### Product Knowledge

Collection of initial set of key product links, which will help in the first 4-6 weeks of onboarding as a new hire.

- **Offerings: GitLab Dedicated vs. GitLab SaaS vs. GitLab Self Managed**
  - [**Self Managed**](https://docs.gitlab.com/ee/subscriptions/self_managed/index.html): In GitLab Self-managed you will have full control over the server itself and the environment
  - [**Dedicated**](https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/index.html)**:**: A single-tenant SaaS service for highly regulated and large enterprises
  - [**SaaS**](https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html): The GitLab software-as-a-service offering. You don’t need to install anything to use GitLab SaaS, you only need to sign up and start using GitLab straight away
- **GitLab Self Managed**
  - [**GitLab Application components**](https://docs.gitlab.com/ee/development/architecture.html#simplified-component-overview) ([details](https://docs.gitlab.com/ee/development/architecture.html#component-list))
  - [**Installation Methods**](https://docs.gitlab.com/ee/install/install_methods.html)
  - [**Reference Architectures**](https://docs.gitlab.com/ee/administration/reference_architectures/)
  - [**Runners**](https://docs.gitlab.com/runner/) (GitLab's "worker" for CI/CD jobs - pipelines)
  - [**Upgrade & Update of GitLab**](https://docs.gitlab.com/ee/update/)
    - [**Upgrade Path**](https://gitlab-com.gitlab.io/support/toolbox/upgrade-path/)
- **Key functionality of GitLab**
  - [**AI**](https://docs.gitlab.com/ee/user/gitlab_duo/index.html)
  - [**CI / CD**](https://docs.gitlab.com/ee/ci/)
  - [**SCM**](https://docs.gitlab.com/ee/user/get_started/get_started_managing_code.html)
- **GitLab Product**
  - [Pricing](https://about.gitlab.com/pricing/)
  - [Find responsible SLACK Channel and Team for GitLab features](https://handbook.gitlab.com/handbook/product/categories/#devops-stages)
  - Stages - Separation inside the product - [Aligned to DevOps](https://handbook.gitlab.com/handbook/product/categories/#devops-stages)
    - https://handbook.gitlab.com/handbook/product/categories/#hierarchy

## GitLab Certifications

At the moment (Feb. 2023) there are 6 [GitLab Technical Certifications - Self-Paced Options](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/gitlab-technical-certifications/). All of those are relevant and great learning material for CSMs at GitLab.
The [Customer Success Manager Onboarding Pathway](https://university.gitlab.com/learn/learning-path/technical-account-manager-tam-onboarding) includes the GitLab Certified [Git Associate](https://university.gitlab.com/learning-paths/certified-git-associate-learning-path), [CI/CD Associate](https://university.gitlab.com/learning-paths/certified-ci-cd-specialist-learning-path), [Project Management Associate](https://university.gitlab.com/learning-paths/gitlab-project-management-associate-learning-path) and [Security Specialist](https://university.gitlab.com/learning-paths/certified-security-specialist-learning-path) learning paths.
So if you are planning on doing **all 4** of those, start directly with the **CSM Onboarding Pathway**.

## GitLab Webinars

As a new joiner to get up to speed on your technical skills and observe how more senior team members hold webinars find the Webinars on the [CSM Scale Webinar calendar](/handbook/customer-success/csm/segment/scale/webinar-calendar/) and register/join as participant.
You can also watch the recordings of some of those Webinars on [YouTube - GitLab Webinars - GitLab Unfiltered - Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

## GitLab.com Playground and Demo space

For self lead training and experimentation you can setup your own playground/demo space on GitLab.com. Find a self-paced guide here:

- [Demo Systems Initial Set Up - Project](https://gitlab.com/gitlab-com/customer-success/demo-engineering/demo-systems-initial-set-up) (Private project for GitLab members only)

## GitLab Self Managed Playgound and Demospace

Many of our prospects and customer choose the option to maintain their own Self Managed GitLab installation based on the reference architecture. To be able to walk in your customers shoes and experience the same possibilities as well as challenges, there is a [shared GitLab Omnibus enviroment](/handbook/customer-success/demo-systems/#shared-environments) for CSMs to use.

1. Follow [these instructions to get access to the shared Omnibus Instances](/handbook/customer-success/demo-systems/#access-shared-omnibus-instances)
1. The [Get started administering GitLab](https://docs.gitlab.com/ee/administration/get_started.html) Guide is a great starting point
1. You can [export](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#manually-upload-service-ping-payload) a [Service Ping](https://docs.gitlab.com/ee/development/service_ping/) data file from the instance and explore product usage data via the [Service ping analysis engine](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/service-ping-analysis-engine)

## Finding Answers

GitLab is a massive and everchanging Product, nobody can know everything. So it is important to learn how to find answers. These handbook pages might help you with it.

1. [Searching GitLab like a pro](/handbook/tools-and-tips/searching/)
1. [Researching Customer Questions](/handbook/customer-success/csm/researching-customer-questions/)
